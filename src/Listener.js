class Listener {
  constructor(service) {
    this._playlistsService = service.playlistsService
    this._mailSender = service.mailSender

    this.listen = this.listen.bind(this)
  }

  async listen(message) {
    try {
      const { playlistId, targetEmail } = JSON.parse(message.content.toString())

      const playlistsongs = await this._playlistsService.getPlaylistSongs(playlistId)
      const result = await this._mailSender.sendEmail(targetEmail, JSON.stringify(playlistsongs))
      console.info('[INFO]: Mail Result ', result)
    } catch (err) {
      console.error(err)
    }
  }
}

module.exports = Listener
