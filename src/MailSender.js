const nodemailer = require('nodemailer')

class MailSender {
  constructor() {
    this._transporter = nodemailer.createTransport({
      host: process.env.MAIL_SERVER,
      port: process.env.MAIL_PORT,
      secure: true,
      auth: {
        user: process.env.MAIL_ADDRESS,
        pass: process.env.MAIL_PASSWORD,
      },
    })
  }

  sendEmail(targetEmail, content) {
    const message = {
      from: 'no-reply@nazuapp.site',
      to: targetEmail,
      subject: 'Export Playlist',
      text: 'The accompanying result of exports Playlists',
      attachments: [
        { filename: 'playlists.json', content },
      ],
    }

    return this._transporter.sendMail(message)
  }
}

module.exports = MailSender
